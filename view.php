<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of contentsimulator.
 *
 * @package     contentsimulator
 * @copyright   2019 Your Name <you@example.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__.'/../../config.php');
require_once(__DIR__.'/lib.php');

// Course_module ID, or
$id = optional_param('id', 0, PARAM_INT);

// ... module instance id.
$c  = optional_param('c', 0, PARAM_INT);

if ($id) {
    $cm             = get_coursemodule_from_id('contentsimulator', $id, 0, false, MUST_EXIST);
    $course         = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moduleinstance = $DB->get_record('contentsimulator', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($c) {
    $moduleinstance = $DB->get_record('contentsimulator', array('id' => $n), '*', MUST_EXIST);
    $course         = $DB->get_record('course', array('id' => $moduleinstance->course), '*', MUST_EXIST);
    $cm             = get_coursemodule_from_instance('contentsimulator', $moduleinstance->id, $course->id, false, MUST_EXIST);
} else {
    print_error(get_string('missingidandcmid', 'contentsimulator'));
}

$userinstanse = $DB->get_record('user', array('id' => $USER->id), '*', MUST_EXIST);
$username = $userinstanse->username;
$useremail = $userinstanse->email;

$contentinstanse = $DB->get_record('registeredcontent', array('id' => $moduleinstance->contentid), '*', MUST_EXIST);
$url = $contentinstanse->url;
$name = $moduleinstance->name;
$contentkey = $contentinstanse->contentkey;

require_login($course, true, $cm);

$modulecontext = context_module::instance($cm->id);


$event = \mod_contentsimulator\event\course_module_viewed::create(array(
    'objectid' => $moduleinstance->id,
    'context' => $modulecontext
));

$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('contentsimulator', $moduleinstance);
$event->trigger();

$PAGE->requires->js('/mod/contentsimulator/launchserver.js','*');

$PAGE->set_url('/mod/contentsimulator/view.php', array('id' => $cm->id));
$PAGE->set_pagelayout('standard');

$PAGE->set_title(format_string($moduleinstance->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($modulecontext);

echo $OUTPUT->header();

echo '<div id="launch-section">';
echo '<span>External Asset: </span>';
echo "<button type='button' class='btn' onclick=\"createLaunch('{$url}','{$username}','{$useremail}','{$contentkey}')\"> Launch {$name} </button>";
echo '</div>';

echo $OUTPUT->footer();
