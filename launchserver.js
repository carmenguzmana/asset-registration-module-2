
 $(document).ready(function(){
    getTasks(1);

    $('#id_assetid').change(function(){
        //Selected value
        getTasks($(this).val());
    });
    
    $('#id_tasks_list').change(function(){
        //Selected value
        setTask($(this).val());
    });
    
    function getTasks(str){
        $('#id_tasks_list').empty();
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //console.log('Response: ',this.responseText);
                var responsJSON = JSON.parse(this.responseText);
                //console.log('SJON: ',responsJSON);
                var x = document.getElementById("id_tasks_list");
                for (var i = 0; i <= responsJSON.tasks.length-1; i++){
                    var option = document.createElement("option");
                    option.text = responsJSON.tasks[i].name;
                    option.value = responsJSON.tasks[i].link;
                    x.add(option);
                }
                if (responsJSON.tasks.length>0){
                    setTask(responsJSON.tasks[0].link)
                }
            }
        };
        xmlhttp.open("GET", "/mod/contentsimulator/tasks.php?id=" + str, true);
        xmlhttp.send();
    }
    
    function setTask(taskURL){
        var xmlhttp = new XMLHttpRequest();
        // xmlhttp.onreadystatechange = function() {
        //     if (this.readyState == 4 && this.status == 200) {
        //         console.log('Response: ',this.responseText);
        //     }
        // };
        xmlhttp.open("GET", "/mod/contentsimulator/store_content.php?url=" + taskURL, true);
        xmlhttp.send();
    }
    

});



function loadJSON(callback) {   

    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', 'config.json', true); 
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
          }
    };
    xobj.send(null);  
 }

function createLaunch(theurl, thename, theemail, contentkey) {
    console.log(theurl, thename, theemail, contentkey);
    loadJSON(function(response) {
        // Parse JSON string into object
        var config = JSON.parse(response);
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open( 'POST',`${config.LS_CREATE_LAUNCH}/${contentkey}` , false); // false for synchronous request
        xmlHttp.setRequestHeader('Access-Control-Allow-Origin', '*');
        xmlHttp.setRequestHeader('Access-Control-Allow-Methods', '*');
        xmlHttp.setRequestHeader('Content-Type', 'application/json');
    
        xmlHttp.send(JSON.stringify(
            { 
                "user":{ 
                    "email": theemail,
                    "name":  thename
                }
            })
        );
        // if (!theurl.startsWith('http://') || !theurl.startsWith('https://') ){
        //     theurl = 'http://' + theurl
        // }
        var jsonObj = JSON.parse(xmlHttp.responseText)

        window.open(`${theurl}/?LaunchToken=${jsonObj.LaunchToken}`);
        return;
        });
    
};

  



