<?php

require(__DIR__.'/../../config.php');
require_once(__DIR__.'/lib.php');

function httpGet($url)
{
    $ch = curl_init();  
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    $output=curl_exec($ch); 
    curl_close($ch);
    return $output;
};

$id = htmlspecialchars($_GET["id"]);

global $DB;
$assetinstanse = $DB->get_record('block_assetregister', array('id' => $id), '*', MUST_EXIST);
$link = $assetinstanse->url;

echo httpGet($link);

?>